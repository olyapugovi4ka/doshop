//
//  ExampleTest.swift
//  DOShopTests
//
//  Created by MacBook on 20.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import XCTest
@testable import DOShop
import  Alamofire

enum ErrorStub: Error {
    case test
}

struct NetworkResponseStub: Codable {
    let userId: Int
    let id: Int
    let title: String
    let body: String
}

class ErrorParserStub2: AbstractErrorParser {
    func parse(_ result: Error) -> Error {
        return ErrorStub.test
    }

    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
        return error
    }

}

class ExampleTest: XCTestCase {

    let expectation = XCTestExpectation(description: "test")
    override func setUpWithError() throws {

    }

    override func tearDownWithError() throws {

    }

    func sum(v1: Int, v2: Int) -> Int {
        return v1 + v2
    }

    func testExample() throws {
        //when
       let  value1 = 2
       let value2 = 5

        //then
       let result = sum(v1: value1, v2: value2)

        //result
        //XCTAssert(result == 7)
        XCTAssertFalse(result != 7)

    }

    func testNetworkRequest() {
        //when
        let url = "https://jsonplaceholder.typicode.com/posts/1"
        let parser = ErrorParserStub2()

        //then

        Alamofire.request(url)
            .responseCodable(errorParser: parser) { (response: DataResponse<NetworkResponseStub>) in
                switch response.result {
                case .success:
                    break
                case .failure:
                    XCTFail()
                }
//            .responseData { response in
//                switch response.result {
//                case .success(_):
//                    break
//                case .failure(_):
//                    XCTFail()
//                }
               self.expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10.0)

        //result

    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
