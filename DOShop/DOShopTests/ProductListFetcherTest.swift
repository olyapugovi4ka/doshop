//
//  ProductListFetcherTest.swift
//  DOShopTests
//
//  Created by MacBook on 16.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import XCTest
@testable import DOShop
import  Alamofire

class ProductListFetcherTest: XCTestCase {

    let requestFactory = RequestFactory()
    let expectation = XCTestExpectation(description: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/catalogData.json")

    override func setUpWithError() throws {

    }

    override func tearDownWithError() throws {

    }

    func testProductListDataAndParse() throws {
        //when
        let productListFetcher = requestFactory.makeProductListFetcherFactory()

        //then
        productListFetcher.fetchProductList(pageNumber: 1, idCategory: 1) { (response) in
            switch response.result {
            case .success:
                break
            case .failure:
                XCTFail()
            }
            self.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)

    }

}
