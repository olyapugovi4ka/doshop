//
//  AuthTest.swift
//  DOShopTests
//
//  Created by MacBook on 16.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import XCTest
@testable import DOShop
import  Alamofire
class AutorizationControllerMock: AutorizationController {

}
class AutorizationRouterMock: AutorizationRouter {

    var suspended = false

    override func goToShop() {
        suspended = true
    }
}

class AuthTest: XCTestCase {

    let requestFactory = RequestFactory()
     let expectation = XCTestExpectation(description:
        "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/login.json")

    override func setUpWithError() throws {

    }

    override func tearDownWithError() throws {

    }

    func testShouldLoginAndParseRequest() {
        //when
        let auth = requestFactory.makeAuthRequestFactory()
        //then
        auth.login(userName: "Somebody", password: "mypassword") { [weak self] response in
            switch response.result {
            case .success:
                break
            case .failure:
                XCTFail()
            }
            self?.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
}
