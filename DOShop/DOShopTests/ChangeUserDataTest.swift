//
//  ChangeUserDataTest.swift
//  DOShopTests
//
//  Created by MacBook on 16.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import XCTest
@testable import DOShop
import  Alamofire

class ChangeUserDataTest: XCTestCase {
    let requestFactory = RequestFactory()
    let expectation = XCTestExpectation(description: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/changeUserData.json")

    override func setUpWithError() throws {

    }

    override func tearDownWithError() throws {

    }

    func testChangeUserDataAndParseRequest() {
        //when
        let changeUserData = requestFactory.makeChangeUserDataFactory()
         //then
        changeUserData.changeUserData(userName: "Somebody",
                                      password: "mypassword",
                                      email: "some@some.ru",
                                      gender: "m",
                                      creditCard: "9872389-2424-234224-234",
                                      bio: "This is good! I think I will switch to another language") { (response) in
            switch response.result {
            case .success:
                break
            case .failure:
                XCTFail()
            }
            self.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }

}
