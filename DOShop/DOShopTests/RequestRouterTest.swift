//
//  RequestRouterTest.swift
//  DOShopTests
//
//  Created by MacBook on 16.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import XCTest
@testable import DOShop
import  Alamofire

enum RequestRouterEncodingStub {
    case url, json
}

struct RequestRouterStub: RequestRouter {
    var baseUrl: URL = URL(
        string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    var method: HTTPMethod = .get
    var path: String = "registerUser.json"
    var parameters: Parameters?
}

class RequestRouterTest: XCTestCase {

    override func setUpWithError() throws {

    }

    override func tearDownWithError() throws {

    }

    func testConvertRequest() throws {
        //when
        let requestRouter = RequestRouterStub()

        //then
        let result = try requestRouter.asURLRequest()

        //result
        XCTAssertNoThrow(result)

    }

}
