//
//  RegisterTest.swift
//  DOShopTests
//
//  Created by MacBook on 16.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import XCTest
@testable import DOShop
import  Alamofire

class RegisterTest: XCTestCase {

    let requestFactory = RequestFactory()
    let expectation = XCTestExpectation(description: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/registerUser.json")

    override func setUpWithError() throws {

    }

    override func tearDownWithError() throws {

    }
    func testRegistrationAndParseRequest() {
        //when
        let registration = requestFactory.makeRegisterRequestFactory()
        //then
        registration.register(userName: "Somebody", password: "mypassword", email: "some@some.ru") { (response) in
            switch response.result {
            case .success:
                break
            case .failure:
                XCTFail()
            }
            self.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }

}
