//
//  AutorizationAssembly.swift
//  DOShop
//
//  Created by MacBook on 18.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

final class AutorizationAssembly {
    
    private let requestFactory = RequestFactory()
    private var viewController: AutorizationController
    init(viewController: AutorizationController) {
        self.viewController = viewController
    }
    
    func assembly() {
        let router = AutorizationRouter(viewController: viewController)
        let presenter = AutorizationPresenter(requestFactory: requestFactory,
                                              viewController: viewController,
                                              router: router)
        
        viewController.presenter = presenter
        
    }
}
