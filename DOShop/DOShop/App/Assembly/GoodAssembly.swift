//
//  GoodAssembly.swift
//  DOShop
//
//  Created by MacBook on 21.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

final class GoodAssembly {
    
    private let requestFactory = RequestFactory()
    private var viewController: GoodController
    init(viewController: GoodController) {
        self.viewController = viewController
    }
    
    func assembly() {
        let router = GoodRouter(viewController: viewController)
        let presenter = GoodPresenter(requestFactory: requestFactory,
                                      viewController: viewController,
                                      router: router )
        
        viewController.presenter = presenter
        
    }
}
