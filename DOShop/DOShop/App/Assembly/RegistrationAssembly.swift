//
//  RegistrationAssembly.swift
//  DOShop
//
//  Created by MacBook on 21.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

final class RegistrationAssembly {
    
    private let requestFactory = RequestFactory()
    private var viewController: RegistrationController
    init(viewController: RegistrationController) {
        self.viewController = viewController
    }
    
    func assembly() {
        let router = RegistrationRouter(viewController: viewController)
        let presenter = RegistrationPresenter(requestFactory: requestFactory,
                                              viewController: viewController,
                                              router: router)
        
        viewController.presenter = presenter
        
    }
}
