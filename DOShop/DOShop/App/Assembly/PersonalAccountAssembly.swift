//
//  PersonalAccountAssembly.swift
//  DOShop
//
//  Created by MacBook on 21.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

final class PersonalAccountAssembly {
    
    private let requestFactory = RequestFactory()
    private var viewController: PersonalAccountController
    init(viewController: PersonalAccountController) {
        self.viewController = viewController
    }
    
    func assembly() {
        let router = PersonalAccountRouter(viewController: viewController)
        let presenter = PersonalAccountPresenter(requestFactory: requestFactory,
                                                 viewController: viewController,
                                                 router: router)
        
        viewController.presenter = presenter
        
    }
}
