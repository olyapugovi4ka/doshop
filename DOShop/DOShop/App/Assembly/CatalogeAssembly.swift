//
//  CatalogeAssembly.swift
//  DOShop
//
//  Created by MacBook on 21.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

final class CatalogeAssembly {
    
    private let requestFactory = RequestFactory()
    private var viewController: CatalogeController
    init(viewController: CatalogeController) {
        self.viewController = viewController
    }
    
    func assembly() {
        let router = CatalogeRouter(viewController: viewController)
        let presenter = CatalogePresenter(requestFactory: requestFactory,
                                          viewController: viewController,
                                          router: router)
        
        viewController.presenter = presenter
        
    }
}
