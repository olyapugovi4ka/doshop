//
//  ChangeUserDataResult.swift
//  DOShop
//
//  Created by MacBook on 13.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

struct ChangeUserDataResult: Codable {
    let result: Int
}
