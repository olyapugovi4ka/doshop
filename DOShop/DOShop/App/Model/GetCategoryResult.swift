//
//  GetCategoryResult.swift
//  DOShop
//
//  Created by ios_Dev on 21.05.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

struct GetCategoryResult: Codable {
    let categories: [Category]
}

struct Category: Codable {
    let id: Int
    let name: String
}
