//
//  ApproveReviewResult.swift
//  DOShop
//
//  Created by MacBook on 26.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

struct ApproveReviewResult: Codable {
    let result: Int
}


