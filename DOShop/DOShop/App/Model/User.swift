//
//  User.swift
//  DOShop
//
//  Created by MacBook on 12.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

struct User: Codable {
    let id: Int
    let userName: String
    let email: String
    let avatar: String? = nil
    
}
