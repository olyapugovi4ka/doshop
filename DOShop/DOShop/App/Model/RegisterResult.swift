//
//  RegisterResult.swift
//  DOShop
//
//  Created by MacBook on 12.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

struct RegisterResult: Codable {
    let accessToken: String
    let refreshToken: String
    let expiresAt: Int? = nil
}
