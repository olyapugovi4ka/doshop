//
//  ProductResult.swift
//  DOShop
//
//  Created by MacBook on 16.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

struct ProductListResult: Codable {
    // swiftlint:disable identifier_name
    let id: Int
    let name: String
    let price: Int

    enum CodingKeys: String, CodingKey {
        case id = "id_product"
        case name = "product_name"
        case price = "price"
    }
    // swiftlint:enable identifier_name
}
