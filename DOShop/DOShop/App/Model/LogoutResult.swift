//
//  LogoutResult.swift
//  DOShop
//
//  Created by MacBook on 12.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation
struct LogoutResult: Codable {
    let result: Int
}
