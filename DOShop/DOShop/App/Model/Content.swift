//
//  Content.swift
//  DOShop
//
//  Created by MacBook on 26.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

struct Content: Codable {
    let productId: Int
    let productName: String
    let price: Int
    let quantity: Int
    enum CodingKeys: String, CodingKey {
        case productId = "id_product"
        case productName = "product_name"
        case price = "price"
        case quantity = "quantity"
    }
}
