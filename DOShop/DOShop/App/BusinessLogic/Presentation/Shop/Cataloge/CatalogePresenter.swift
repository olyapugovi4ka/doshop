//
//  CatalogePresenter.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

protocol CatalogePresenterProtocol: class {
    func loadProductList()
    // swiftlint:disable identifier_name
    func goToGoodController(with id: Int)
    // swiftlint:enable identifier_name
}

class CatalogePresenter {

    private let requestFactory: RequestFactoryProtocol
     private var viewController: CatalogeController
     private var router: CatalogeRouterProtocol

     init(requestFactory: RequestFactoryProtocol,
          viewController: CatalogeController,
          router: CatalogeRouterProtocol) {
         self.requestFactory = requestFactory
         self.viewController = viewController
         self.router = router
     }
}

extension CatalogePresenter: CatalogePresenterProtocol {
    func loadProductList() {
        let productListFetcher = requestFactory.makeGoodRequestFactory()
        productListFetcher.fetchProductList(pageNumber: 1, idCategory: 1) { (response) in
            switch response.result {
            case .success(let productList):
                print(productList)
                DispatchQueue.main.async {
                    self.viewController.productList = productList
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
// swiftlint:disable identifier_name
    func goToGoodController(with id: Int) {
        self.router.goToGood(with: id)
    }
// swiftlint:enable identifier_name
}
