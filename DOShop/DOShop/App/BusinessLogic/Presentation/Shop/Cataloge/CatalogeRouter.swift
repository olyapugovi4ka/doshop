//
//  CatalogeRouter.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

protocol CatalogeRouterProtocol {
    // swiftlint:disable identifier_name
    func goToGood(with id: Int)
}
class CatalogeRouter {

    private var viewController: UIViewController

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension CatalogeRouter: CatalogeRouterProtocol {
    // swiftlint:disable force_cast
    func goToGood(with id: Int) {
        let goodController = UIStoryboard(name: "GoodController", bundle: nil)
            .instantiateInitialViewController() as! GoodController
         // swiftlint:enable force_cast
        goodController.assembler = GoodAssembly(viewController: goodController)
        goodController.id = id
        viewController.navigationController?.pushViewController(goodController, animated: true)
    }
}
