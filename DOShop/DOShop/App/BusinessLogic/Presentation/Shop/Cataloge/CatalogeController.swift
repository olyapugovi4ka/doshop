//
//  CatalogeController.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

class CatalogeController: UIViewController {

    var presenter: CatalogePresenterProtocol?
    var assembler: CatalogeAssembly!

    var productList = [ProductListResult]() {
        didSet {
            self.tableView.reloadData()
        }
    }

    //outlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        assembler.assembly()
        presenter?.loadProductList()

    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }

}

extension CatalogeController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let product = productList[indexPath.row]
        cell.textLabel?.text = product.name
        cell.detailTextLabel?.text = String(describing: product.price)

        return cell
    }

}
extension CatalogeController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // swiftlint:disable identifier_name
        let id = productList[indexPath.row].id
         // swiftlint:enable identifier_name
        presenter?.goToGoodController(with: id)
    }
}
