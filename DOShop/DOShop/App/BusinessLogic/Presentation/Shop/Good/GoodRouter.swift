//
//  GoodRouter.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

protocol GoodRouterProtocol {
    func goTo()
}

class GoodRouter {

    private var viewController: UIViewController

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension GoodRouter: GoodRouterProtocol {
    func goTo() {
        print(#function)
    }
}
