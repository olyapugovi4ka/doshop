//
//  GoodPresenter.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

protocol  GoodPresenterProtocol: class {
   // swiftlint:disable identifier_name
    func loadGoodDescription(with id: Int)
    // swiftlint:enable identifier_name
}

class GoodPresenter {

   private let requestFactory: RequestFactoryProtocol
   private var viewController: GoodController
   private var router: GoodRouterProtocol

   init(requestFactory: RequestFactoryProtocol,
        viewController: GoodController,
        router: GoodRouterProtocol) {
       self.requestFactory = requestFactory
       self.viewController = viewController
       self.router = router
   }
}

extension GoodPresenter: GoodPresenterProtocol {
    // swiftlint:disable identifier_name
    func loadGoodDescription(with id: Int) {
                let getGoodByld = requestFactory.makeGoodRequestFactory()
                getGoodByld.getGoodByld(id: id) { (response) in
                    switch response.result {
                    case .success(let goodInfo):
                        print(goodInfo)
                        DispatchQueue.main.async {
                            self.viewController.displayData(with: goodInfo)
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
    }
  // swiftlint:enable identifier_name
}
