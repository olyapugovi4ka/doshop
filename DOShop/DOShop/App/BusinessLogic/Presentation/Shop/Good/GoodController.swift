//
//  GoodController.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

protocol GoodControllerProtocol: class {
    func displayData(with product: ProductResult)
}

class GoodController: UIViewController {

   var presenter: GoodPresenterProtocol?
   var assembler: GoodAssembly!
// swiftlint:disable identifier_name
    var id: Int = 0
    var goodByld: ProductResult?

    //outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!

    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        assembler.assembly()
        presenter?.loadGoodDescription(with: self.id)

    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }

}
extension GoodController: GoodControllerProtocol {
    func displayData(with product: ProductResult) {
        nameLabel.text = product.name
    }

}
