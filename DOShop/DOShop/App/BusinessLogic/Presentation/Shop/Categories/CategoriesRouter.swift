//
//  CategoriesRouter.swift
//  DOShop
//
//  Created by ios_Dev on 22.05.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

protocol CategoriesRouterProtocol {
    func goToProductList(with categoryId: Int)
}

class CategoriesRouter {
    private var viewController: UIViewController

       init(viewController: UIViewController) {
           self.viewController = viewController
       }
}
extension CategoriesRouter: CategoriesRouterProtocol {
    func goToProductList(with categoryId: Int) {
        print(#function)
    }
    
    
}
