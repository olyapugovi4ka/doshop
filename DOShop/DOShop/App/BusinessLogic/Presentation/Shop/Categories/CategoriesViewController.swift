//
//  CategoriesViewController.swift
//  DOShop
//
//  Created by ios_Dev on 22.05.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController {
    
    var categories = [Category]()  {
        
        didSet {
            self.collectionView.reloadData()
        }
    }
   

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    


}

extension CategoriesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        return cell
    }
    
    
}
