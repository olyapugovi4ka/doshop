//
//  CategoriesPresenter.swift
//  DOShop
//
//  Created by ios_Dev on 22.05.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

protocol CategoriesPresenterProtocol {
    func loadCategories()
    func searchCategory()
}

class CategoriesPresenter {
    private let requestFactory: RequestFactoryProtocol
       private var viewController: CategoriesViewController
       private var router: CategoriesRouterProtocol

       init(requestFactory: RequestFactoryProtocol,
            viewController: CategoriesViewController,
            router: CategoriesRouterProtocol) {
           self.requestFactory = requestFactory
           self.viewController = viewController
           self.router = router
       }

}

extension CategoriesPresenter: CategoriesPresenterProtocol {
    func loadCategories() {
        let getCategory = requestFactory.makeGetCategoryRequestFactory()
        getCategory.getCategory { response in
            switch response.result {
            case .success(let value):
                let categories = value.categories
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func searchCategory() {
        print(#function)
    }
    
    
}
