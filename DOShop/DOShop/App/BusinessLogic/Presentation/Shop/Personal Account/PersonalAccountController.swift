//
//  PersonalAccountController.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

class PersonalAccountController: UIViewController {

    var presenter: PersonalAccountPresenterProtocol?
    var assembler: PersonalAccountAssembly!

    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        assembler.assembly()
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }

    //actions
    @IBAction func changeAction(_ sender: Any) {

    }
    @IBAction func sighOutAction(_ sender: Any) {
    }
}
