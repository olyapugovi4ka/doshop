//
//  PersonalAccountRouter.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

protocol  PersonalAccountRouterProtocol {
   func goToRegistration()
}

class PersonalAccountRouter {

    private var viewController: UIViewController

    init(viewController: UIViewController) {
        self.viewController = viewController
    }

}

extension PersonalAccountRouter: PersonalAccountRouterProtocol {
    func goToRegistration() {

    }
}
