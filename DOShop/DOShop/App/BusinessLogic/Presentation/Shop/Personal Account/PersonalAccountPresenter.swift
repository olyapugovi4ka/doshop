//
//  PersonalAccountPresenter.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

protocol  PersonalAccountPresenterProtocol: class {
    func changeUserData()
}

class PersonalAccountPresenter {
    private let requestFactory: RequestFactoryProtocol
    private var viewController: PersonalAccountController
    private var router: PersonalAccountRouterProtocol

    init(requestFactory: RequestFactoryProtocol,
         viewController: PersonalAccountController,
         router: PersonalAccountRouterProtocol) {
        self.requestFactory = requestFactory
        self.viewController = viewController
        self.router = router
    }
}
extension PersonalAccountPresenter: PersonalAccountPresenterProtocol {
    func changeUserData() {

    }
}
