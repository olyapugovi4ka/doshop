//
//  MainTabbarController.swift
//  DOShop
//
//  Created by MacBook on 21.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

class MainTabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let personalAccountController = UIStoryboard(name: "PersonalAccount", bundle: nil).instantiateInitialViewController() as! PersonalAccountController
        personalAccountController.assembler = PersonalAccountAssembly(viewController: personalAccountController)
        personalAccountController.tabBarItem.title = "Personal Account"
        personalAccountController.tabBarItem.image = #imageLiteral(resourceName: "icons8-пользователь-без-половых-признаков-50")

        let catalogeController = UIStoryboard(name: "Cataloge", bundle: nil).instantiateInitialViewController() as! CatalogeController
        catalogeController.assembler = CatalogeAssembly(viewController: catalogeController)
        catalogeController.tabBarItem.title = "Cataloge"
        catalogeController.tabBarItem.image = #imageLiteral(resourceName: "icons8-мобильный-шоппинг-50")

        viewControllers = [personalAccountController, catalogeController]

    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }

}
