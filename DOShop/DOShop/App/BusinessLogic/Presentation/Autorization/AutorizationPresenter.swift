//
//  AuthorizationPresenter.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire
import UIKit

@objc protocol AutorizationPresenterProtocol: class {
    func login(userName: String, password: String)
    func goToRegistration()

}

class AutorizationPresenter {
    private let requestFactory: RequestFactoryProtocol
    private var viewController: AutorizationController
    private var router: AutorizationRouterProtocol

    init(requestFactory: RequestFactoryProtocol,
         viewController: AutorizationController,
         router: AutorizationRouterProtocol) {
        self.requestFactory = requestFactory
        self.viewController = viewController
        self.router = router
    }

}

extension AutorizationPresenter: AutorizationPresenterProtocol {

    func login(userName: String, password: String) {
        let auth = requestFactory.makeAuthRequestFactory()
        auth.login(userName: userName, password: password) { (response) in
            switch response.result {

            case .success(let login):
                print(login)
                DispatchQueue.main.async {
                    self.router.goToShop()
                }

            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    func goToRegistration() {
        print(#function)
        self.router.goToRegistration()
    }

}
