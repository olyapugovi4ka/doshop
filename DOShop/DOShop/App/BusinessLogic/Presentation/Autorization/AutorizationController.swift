//
//  AutorizationController.swift
//  DOShop
//
//  Created by MacBook on 09.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

class AutorizationController: UIViewController {

    //let net = Networking()
  var presenter: AutorizationPresenterProtocol?
   var assembler: AutorizationAssembly!
    //outlets
    @IBOutlet weak var loginTextfield: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        assembler.assembly()
       // net.fetch()
    }

  //actions
    @IBAction func loginAction(_ sender: Any) {
        presenter?.login(userName: "user", password: "12341234")
    }
    @IBAction func passwordAction(_ sender: Any) {
    }

    @IBAction func sighInButtonTapped(_ sender: Any) {
    }

    @IBAction func registrationButtonTapped(_ sender: Any) {
        presenter?.goToRegistration()
    }

}
