//
//  AuthorizationRouter.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

protocol AutorizationRouterProtocol {
    func goToRegistration()
    func goToShop()
}

class AutorizationRouter: AutorizationRouterProtocol {
    
    private var viewController: UIViewController
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func goToRegistration() {
      
        let registrationController = UIStoryboard(name: "RegistrationController", bundle: nil)
            .instantiateInitialViewController() as! RegistrationController
        registrationController.assembler = RegistrationAssembly(viewController: registrationController)
        viewController.navigationController?.pushViewController(registrationController, animated: true)
        
    }
    
    func goToShop() {
        
        let mainTabbarController = MainTabbarController()
        viewController.navigationController?.pushViewController(mainTabbarController, animated: true)
    }
    
}
