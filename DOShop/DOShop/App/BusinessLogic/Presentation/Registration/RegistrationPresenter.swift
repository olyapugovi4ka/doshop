//
//  RegistrationPresenter.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

@objc protocol  RegistrationPresenterProtocol: class {
    func saveData()

}

class RegistrationPresenter {
  private let requestFactory: RequestFactoryProtocol
    private var viewController: RegistrationController
    private var router: RegistrationRouterProtocol

    init(requestFactory: RequestFactoryProtocol,
         viewController: RegistrationController,
         router: RegistrationRouterProtocol) {
        self.requestFactory = requestFactory
        self.viewController = viewController
        self.router = router
    }

}

extension RegistrationPresenter: RegistrationPresenterProtocol {
    func saveData() {

        let registration = requestFactory.makeAuthRequestFactory()
        registration.register(userName: "user",
                              password: "12341234",
                              email: "user@mail.ru") { (response) in
            switch response.result {
            case .success:
                DispatchQueue.main.async {
                    self.router.goToShop()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

}
