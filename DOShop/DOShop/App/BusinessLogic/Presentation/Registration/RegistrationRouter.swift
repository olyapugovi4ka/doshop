//
//  RegistrationRouter.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit
protocol RegistrationRouterProtocol {
    func goToShop()
}
class RegistrationRouter {

    private var viewController: UIViewController

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension RegistrationRouter: RegistrationRouterProtocol {
    func goToShop() {
     let mainTabbarController = MainTabbarController()
        viewController.navigationController?.pushViewController(mainTabbarController, animated: true)
    }

}
