//
//  RegistrationController.swift
//  DOShop
//
//  Created by MacBook on 17.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

class RegistrationController: UIViewController {

   var presenter: RegistrationPresenterProtocol?
    var assembler: RegistrationAssembly!

  // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        assembler.assembly()

    }

//actions
    @IBAction func saveAction(_ sender: Any) {
        presenter?.saveData()
    }
}
