//
//  BasketRequest.swift
//  DOShop
//
//  Created by MacBook on 26.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

class BasketRequest: AbstractRequestFactory{
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(
        string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!

    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility) ) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension BasketRequest: BasketRequestFactory {
    func getBasket(userId: Int, completionHandler: @escaping (DataResponse<GetBasketResult>) -> Void) {
        let requestModel = GetBasketRequest(baseUrl: baseUrl, userId: userId)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
    func addToBasket(productId: Int,
                     quantity: Int,
                     completionHandler: @escaping (DataResponse<AddToBasketResult>) -> Void) {
        let requestModel = AddToBasketRequest(baseUrl: baseUrl,
                                              quantity: quantity,
                                              productId: productId)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
    func deleteFromBasket(productId: Int, completionHandler: @escaping (DataResponse<DeleteFromBasketResult>) -> Void) {
        let requestModel = DeleteFromBasketRequest(baseUrl: baseUrl, productId: productId)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension BasketRequest {
    
    struct GetBasketRequest: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod = .get
        var path: String = "getBasket.json"
        let userId: Int
        var parameters: Parameters? {
            return ["id_user": userId]
        }
    }
    
    struct AddToBasketRequest: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod = .post
        var path: String = "addToBasket.json"
        let quantity: Int
        let productId: Int
        var parameters: Parameters? {
            return ["id_product": productId,
                    "quantity": quantity]
        }
    }
    
    struct DeleteFromBasketRequest: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod = .post
        var path: String = "deleteFromBasket.json"
        let productId: Int
        var parameters: Parameters? {
            return ["id_product": productId]
        }
    }
}
