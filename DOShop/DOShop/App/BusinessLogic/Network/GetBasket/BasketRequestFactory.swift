//
//  BasketRequestFactory.swift
//  DOShop
//
//  Created by MacBook on 26.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

protocol BasketRequestFactory {
    func getBasket(userId: Int, completionHandler: @escaping (DataResponse<GetBasketResult>) -> Void)
    
    func addToBasket(productId: Int, quantity: Int, completionHandler: @escaping (DataResponse<AddToBasketResult>) -> Void)
    
    func deleteFromBasket (productId: Int, completionHandler: @escaping (DataResponse<DeleteFromBasketResult>) -> Void)
}
