//
//  RequestFactory.swift
//  DOShop
//
//  Created by MacBook on 12.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

protocol RequestFactoryProtocol {
    func makeAuthRequestFactory() -> AuthRequestFactory
    func makeChangeUserDataFactory() -> ChangeUserDataRequestFactory
    func makeGoodRequestFactory() -> GoodRequestFactory
    func makeReviewRequestFactory() -> ReviewRequestFactory
    func makeBasketRequestFactory() -> BasketRequestFactory
    func makeGetCategoryRequestFactory() -> GetCategoryRequestFactory
}

class RequestFactory: RequestFactoryProtocol {
    func makeErrorParser() -> AbstractErrorParser {
        return ErrorParser()
    }

    lazy var commonSessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpShouldSetCookies = false
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        let manager = SessionManager(configuration: configuration)
        return manager
    }()

    let sessionQueue = DispatchQueue.global(qos: .utility)

    //auth, register, logout
    func makeAuthRequestFactory() -> AuthRequestFactory {
        let errorParser = makeErrorParser()
        return Auth(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }

    //changeUserData
    func makeChangeUserDataFactory() -> ChangeUserDataRequestFactory {
        let errorParser = makeErrorParser()
        return ChangeUserData(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }

    //good, goodList
    func makeGoodRequestFactory() -> GoodRequestFactory {
        let errorParser = makeErrorParser()
        return GoodRequest(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }

    //addreview, approveReview, removeReview
    func makeReviewRequestFactory() -> ReviewRequestFactory {
        let errorParser = makeErrorParser()
        return ReviewRequest(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
   
    //getBasket, addToBaket, deleteFromBasket
    func makeBasketRequestFactory() -> BasketRequestFactory {
        let errorParser = makeErrorParser()
        return BasketRequest(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
    
    //getCategories
    func makeGetCategoryRequestFactory() -> GetCategoryRequestFactory {
        let errorParser = makeErrorParser()
        return GetCategory(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
}
