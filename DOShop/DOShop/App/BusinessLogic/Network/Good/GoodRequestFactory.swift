//
//  GoodRequestFactory.swift
//  DOShop
//
//  Created by MacBook on 16.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

protocol GoodRequestFactory {

func fetchProductList(pageNumber: Int,
                      idCategory: Int,
                      completionHandler: @escaping (DataResponse<[ProductListResult]>) -> Void )
    
    func getGoodByld(id: Int, completionHandler: @escaping (DataResponse<ProductResult>) -> Void)
}
