//
//  GoodRequest.swift
//  DOShop
//
//  Created by MacBook on 16.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

class GoodRequest: AbstractRequestFactory {
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
   var baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!

    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }

}
extension GoodRequest: GoodRequestFactory {
    func fetchProductList(pageNumber: Int,
                          idCategory: Int,
                          completionHandler: @escaping (DataResponse<[ProductListResult]>) -> Void) {
        let requestModel = GoodList(baseUrl: baseUrl,
                                       pageNumber: pageNumber,
                                       idCategory: idCategory)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
    func getGoodByld(id: Int,
                     completionHandler: @escaping (DataResponse<ProductResult>) -> Void) {
        let requestModel = Good(baseUrl: baseUrl, id: id)
        self.request(request: requestModel, completionHandler: completionHandler)
    }

}

extension GoodRequest {
    
    struct GoodList: RequestRouter {
        
        var baseUrl: URL
        var method: HTTPMethod = .get
        var path: String = "catalogData.json"
        let pageNumber: Int
        let idCategory: Int
        var parameters: Parameters? {
            return ["page_number": pageNumber,
                    "id_category": idCategory]
        }
    }
    
    struct Good: RequestRouter {
        
        var baseUrl: URL
        var method: HTTPMethod = .get
        var path: String = "getGoodById.json"
        let id: Int
        var parameters: Parameters? {
            return ["id_product": id]
        }
        
    }
}
