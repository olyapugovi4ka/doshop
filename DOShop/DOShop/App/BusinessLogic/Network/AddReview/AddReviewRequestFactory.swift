//
//  ReviewRequestFactory.swift
//  DOShop
//
//  Created by MacBook on 26.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

protocol ReviewRequestFactory {
    
    func addReview(userId: Int,
                   reviewText: String,
                   completionHandler: @escaping (DataResponse<AddReviewResult>) -> Void)
    
    func approveReview(commentId: Int,
                       completionHandler: @escaping (DataResponse<ApproveReviewResult>) -> Void)
    
    func removeReview(commentId: Int,
                      completionHandler: @escaping (DataResponse<RemoveReviewResult>) -> Void)
}
