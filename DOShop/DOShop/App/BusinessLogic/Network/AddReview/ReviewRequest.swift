//
//  ReviewRequest.swift
//  DOShop
//
//  Created by MacBook on 26.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

class ReviewRequest: AbstractRequestFactory {
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(
        string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility) ) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension ReviewRequest: ReviewRequestFactory {
    func addReview(userId: Int,
                   reviewText: String,
                   completionHandler: @escaping (DataResponse<AddReviewResult>) -> Void) {
        let requestModel = AddReviewRequest(baseUrl: baseUrl,
                                            userId: userId,
                                            reviewText: reviewText)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
    func approveReview(commentId: Int,
                       completionHandler: @escaping (DataResponse<ApproveReviewResult>) -> Void) {
        let requestModel = ApproveReviewRequest(baseUrl: baseUrl, commentId: commentId)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
    func removeReview(commentId: Int, completionHandler: @escaping (DataResponse<RemoveReviewResult>) -> Void) {
        let requestModel = RemoveReviewRequest(baseUrl: baseUrl, commentId: commentId)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension ReviewRequest {
    
    struct AddReviewRequest: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod = .post
        var path: String = "addReview.json"
        let userId: Int
        let reviewText: String
        var parameters: Parameters? {
            return ["id_user": userId,
                    "text": reviewText]
        }
    }
    
    struct ApproveReviewRequest: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod = .post
        var path: String = "approveReview.json"
        let commentId: Int
        var parameters: Parameters? {
            return ["id_comment": commentId]
        }
    }
    
    struct RemoveReviewRequest: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod = .post
        var path: String = "removeReview.json"
        let commentId: Int
        var parameters: Parameters? {
            return ["id_comment": commentId]
        }
    }
}
