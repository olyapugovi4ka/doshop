//
//  AuthRequestFactory.swift
//  DOShop
//
//  Created by MacBook on 12.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

protocol AuthRequestFactory {
    
    func login(userName: String,
               password: String,
               completionHandler: @escaping (DataResponse<LoginResult>) -> Void)
    
    func logout(completionHandler: @escaping (DataResponse<LogoutResult>) -> Void)
    
    func register(userName: String,
                  password: String,
                  email: String,
                  completionHandler: @escaping (DataResponse<RegisterResult>) -> Void)
    
}
