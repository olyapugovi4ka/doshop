//
//  GetCategoriesFactory.swift
//  DOShop
//
//  Created by ios_Dev on 21.05.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

protocol GetCategoryRequestFactory {
    func getCategory(completionHandler: @escaping(DataResponse<GetCategoryResult>) -> Void)
}
