//
//  GetCategory.swift
//  DOShop
//
//  Created by ios_Dev on 21.05.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

class GetCategory: AbstractRequestFactory {
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(
        string: "http://localhost:8080")!

    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility) ) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension GetCategory: GetCategoryRequestFactory {
    func getCategory(completionHandler: @escaping (DataResponse<GetCategoryResult>) -> Void) {
        let requestModel = CategoryRequest(baseUrl: baseUrl)
        self.request(request: requestModel, completionHandler: completionHandler)
    }

}

extension GetCategory {
    struct CategoryRequest: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "/category/get_categories"
        var parameters: Parameters? {
            return nil
        }

    }
}
