//
//  ChangeUserDataRequestFactory.swift
//  DOShop
//
//  Created by MacBook on 13.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire

protocol ChangeUserDataRequestFactory {

    func changeUserData(userName: String,
                        password: String,
                        email: String,
                        gender: String,
                        creditCard: String,
                        bio: String,
                        completionHandler: @escaping (DataResponse<ChangeUserDataResult>) -> Void)
}


