//
//  ChangeUserData.swift
//  DOShop
//
//  Created by MacBook on 13.04.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Alamofire
//changeUserData.json

class ChangeUserData: AbstractRequestFactory {
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(
        string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!

    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }

}

extension ChangeUserData: ChangeUserDataRequestFactory {
    // swiftlint:disable function_parameter_count
    func changeUserData(userName: String,
                        password: String,
                        email: String,
                        gender: String,
                        creditCard: String,
                        bio: String,
                        completionHandler: @escaping (DataResponse<ChangeUserDataResult>) -> Void) {
        let requestModel = ChangeUserDataStruct(baseUrl: baseUrl,
                                                login: userName,
                                                password: password,
                                                email: email,
                                                gender: gender,
                                                creditCard: creditCard,
                                                bio: bio)
        // swiftlint:enable function_parameter_count
        self.request(request: requestModel, completionHandler: completionHandler)
    }

}
extension ChangeUserData {
    enum Gender: String {
        case man = "m"
        case woman = "w"
    }
    struct ChangeUserDataStruct: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod = .get
        var path: String = "changeUserData.json"
        let idUser: Int = 123
        let login: String
        let password: String
        let email: String
        let gender: Gender.RawValue
        let creditCard: String
        let bio: String
        var parameters: Parameters? {
            return ["id_user": idUser,
                    "username": login,
                    "password": password,
                    "email": email,
                    "gender": gender,
                    "credit_card": creditCard,
                    "bio": bio]
        }

    }
}
